#include <CL/sycl.hpp>
#include <dpct/dpct.hpp>
#include <string>

extern "C" {
#include "png_util.h"
#include <chrono>
}

void writeImage(std::string filename, int NRe, int NIm, float *h_count) {
  FILE *png = fopen(filename.c_str(), "w");
  write_hot_png(png, NRe, NIm, h_count, 0, 80);
  fclose(png);
}

template <int iters, int NX, int NY>
void mandelbrotV2(const int NRe, const int NIm, const float minRe,
                             const float minIm, const float dRe,
                             const float dIm, float *hCount,
                             sycl::nd_item<3> item_ct1,
                             sycl::accessor<float, 2, sycl::access_mode::read_write, sycl::access::target::local> s_A) {

  int nre = item_ct1.get_local_id(2) +
            item_ct1.get_group(2) * item_ct1.get_local_range().get(2);
  int nim = item_ct1.get_local_id(1) +
            item_ct1.get_group(1) * item_ct1.get_local_range().get(1);

  if (nim < NIm && nre < NRe) {
    float cRe = minRe + nre * dRe;
    float cIm = minIm + nim * dIm;
    float zRe = 2.f * cRe;
    float zIm = 2.f * cIm;

    int i = 0;
    float a, b;

    do {
      a = zRe * zRe + cRe;
      b = cRe - zIm * zIm;

      zIm = cIm + zRe * zIm;
      zIm *= 2;

      zRe = a + b;
      i++;
    } while (i < iters && a - b < 4.f);

    s_A[item_ct1.get_local_id(2)][item_ct1.get_local_id(1)] = i;
  }

  /*
  DPCT1065:0: Consider replacing sycl::nd_item::barrier() with
  sycl::nd_item::barrier(sycl::access::fence_space::local_space) for better
  performance if there is no access to global memory.
  */
  item_ct1.barrier();

  if (nim < NIm && nre < NRe) {
    hCount[nim * NIm + nre] =
        s_A[item_ct1.get_local_id(2)][item_ct1.get_local_id(1)];
  }
}

template <int iters>
void mandelbrot(const int NRe, const int NIm, const float minRe,
                           const float minIm, const float dRe, const float dIm,
                           float *hCount, sycl::nd_item<3> item_ct1) {
  int nre = item_ct1.get_local_id(2) +
            item_ct1.get_group(2) * item_ct1.get_local_range().get(2);
  int nim = item_ct1.get_local_id(1) +
            item_ct1.get_group(1) * item_ct1.get_local_range().get(1);

  if (nim < NIm && nre < NRe) {
    float cRe = 2.f * (minRe + nre * dRe);
    float cIm = 2.f * (minIm + nim * dIm);
    float zRe = 0;
    float zIm = 0;
    int i = 0;

    for (i = 0; i < iters; i++) {
      float zReTmp = zRe * zRe - zIm * zIm + cRe;
      zIm = 2.f * zIm * zRe + cIm;
      zRe = zReTmp;

      if (zRe * zRe + zIm * zIm > 4.f) {
        break;
      }
    }

    hCount[nim * NIm + nre] = i;
  }
}

int main(int argc, char const *argv[]) {
  dpct::dev_mgr::instance().select_device(1);
  const int blockSize = 16;
  const int NRe = 4096;
  const int NIm = 4096;

  const int iters = 400;

  /* box containing sample points */
  const float centRe = .133, centIm = -.635;
  const float diam = 0.04;
  const float minRe = (centRe - 0.5 * diam) / 2.f;
  const float maxRe = centRe + 0.5 * diam;
  const float minIm = (centIm - 0.5 * diam) / 2.f;
  const float maxIm = centIm + 0.5 * diam;

  const float dRe = (maxRe - 2.f * minRe) / (NRe - 1.f) / 2.f;
  const float dIm = (maxIm - 2.f * minIm) / (NIm - 1.f) / 2.f;

  float *h_count = (float *)calloc(NRe * NIm, sizeof(float));
  float *d_count;
  d_count = sycl::malloc_device<float>(NRe * NIm, dpct::get_default_queue());

  sycl::range<3> B(1, blockSize, blockSize);
  sycl::range<3> G(1, (NIm + blockSize - 1) / blockSize,
                   (NRe + blockSize - 1) / 16);

  sycl::event tic, toc;
  std::chrono::time_point<std::chrono::steady_clock> tic_ct1;
  std::chrono::time_point<std::chrono::steady_clock> toc_ct1;
  /*
  DPCT1026:1: The call to cudaEventCreate was removed because this call is
  redundant in DPC++.
  */
  /*
  DPCT1026:2: The call to cudaEventCreate was removed because this call is
  redundant in DPC++.
  */
  dpct::get_current_device().queues_wait_and_throw();
  /*
  DPCT1012:3: Detected kernel execution time measurement pattern and generated
  an initial code for time measurements in SYCL. You can change the way time is
  measured depending on your goals.
  */
  tic_ct1 = std::chrono::steady_clock::now();

  /*
  DPCT1049:4: The workgroup size passed to the SYCL kernel may exceed the limit.
  To get the device limit, query info::device::max_work_group_size. Adjust the
  workgroup size if needed.
  */
  toc = dpct::get_default_queue().parallel_for(
      sycl::nd_range<3>(G * B, B), [=](sycl::nd_item<3> item_ct1) {
        mandelbrot<iters>(NRe, NIm, minRe, minIm, dRe, dIm, d_count, item_ct1);
      });

  /*
  DPCT1012:5: Detected kernel execution time measurement pattern and generated
  an initial code for time measurements in SYCL. You can change the way time is
  measured depending on your goals.
  */
  toc.wait();
  toc_ct1 = std::chrono::steady_clock::now();
  dpct::get_current_device().queues_wait_and_throw();

  float gpuTime;
  gpuTime = std::chrono::duration<float, std::milli>(toc_ct1 - tic_ct1).count();
  gpuTime /= 1000.0;

  printf("Mandelbrot elapsed time: %g\n", gpuTime);
  dpct::get_default_queue()
      .memcpy(h_count, d_count, NRe * NIm * sizeof(float))
      .wait();
  writeImage("cudaMandelbrotV1.png", NRe, NIm, h_count);

  dpct::get_current_device().queues_wait_and_throw();
  /*
  DPCT1012:6: Detected kernel execution time measurement pattern and generated
  an initial code for time measurements in SYCL. You can change the way time is
  measured depending on your goals.
  */
  tic_ct1 = std::chrono::steady_clock::now();

  /*
  DPCT1049:7: The workgroup size passed to the SYCL kernel may exceed the limit.
  To get the device limit, query info::device::max_work_group_size. Adjust the
  workgroup size if needed.
  */
  toc = dpct::get_default_queue().submit([&](sycl::handler &cgh) {
    sycl::accessor<float, 2, sycl::access_mode::read_write,
                   sycl::access::target::local>
        s_A_acc_ct1(sycl::range<2>(blockSize, blockSize), cgh);

    cgh.parallel_for(
        sycl::nd_range<3>(G * B, B), [=](sycl::nd_item<3> item_ct1) {
          mandelbrotV2<iters, blockSize, blockSize>(
              NRe, NIm, minRe, minIm, dRe, dIm, d_count, item_ct1, s_A_acc_ct1);
        });
  });

  /*
  DPCT1012:8: Detected kernel execution time measurement pattern and generated
  an initial code for time measurements in SYCL. You can change the way time is
  measured depending on your goals.
  */
  toc.wait();
  toc_ct1 = std::chrono::steady_clock::now();
  dpct::get_current_device().queues_wait_and_throw();

  gpuTime = std::chrono::duration<float, std::milli>(toc_ct1 - tic_ct1).count();
  gpuTime /= 1000.0;

  printf("Mandelbrot v2 elapsed time: %g\n", gpuTime);
  dpct::get_default_queue()
      .memcpy(h_count, d_count, NRe * NIm * sizeof(float))
      .wait();
  writeImage("cudaMandelbrotV2.png", NRe, NIm, h_count);

  sycl::free(d_count, dpct::get_default_queue());
}
