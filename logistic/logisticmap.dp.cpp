#include <CL/sycl.hpp>
#include <dpct/dpct.hpp>
#include <fstream>
#include <iostream>

template <int Nit>
void logisticMap(int N, float *r, float *x, sycl::nd_item<3> item_ct1) {
  int t = item_ct1.get_local_id(2);
  int b = item_ct1.get_group(2);
  int B = item_ct1.get_local_range().get(2);

  int n = t + b * B;

  if (n < N) {
    float x_n = x[n];
    float r_n = r[n];

    for (int i = 0; i < Nit; i++) {
      x_n = r_n * x_n * (1.f - x_n);
    }

    x[n] = x_n;
  }
}

int main(int argc, char const *argv[]) {
  dpct::dev_mgr::instance().select_device(1);

  float rMin = 0.f;
  float rMax = 4.f;
  int N = 2000000;       // num r vals to run on
  const int Nit = 3000;  // iterations of the map per val

  float *h_r = (float *)malloc(N * sizeof(float));
  float *h_x = (float *)malloc(N * sizeof(float));

  float *c_r, *c_x;
  c_r = sycl::malloc_device<float>(N, dpct::get_default_queue());
  c_x = sycl::malloc_device<float>(N, dpct::get_default_queue());

  srand48(1234567);
  for (int i = 0; i < N; i++) {
    h_r[i] = rMin + i / (N - 1.f) * (rMax - rMin);
    h_x[i] = (float)drand48();
  }

  dpct::get_default_queue().memcpy(c_r, h_r, N * sizeof(float));
  dpct::get_default_queue().memcpy(c_x, h_x, N * sizeof(float)).wait();

  int T = 256;
  int G = (N + T - 1) / T;
  /*
  DPCT1049:0: The workgroup size passed to the SYCL kernel may exceed the limit.
  To get the device limit, query info::device::max_work_group_size. Adjust the
  workgroup size if needed.
  */
  dpct::get_default_queue().parallel_for(
      sycl::nd_range<3>(sycl::range<3>(1, 1, G) * sycl::range<3>(1, 1, T),
                        sycl::range<3>(1, 1, T)),
      [=](sycl::nd_item<3> item_ct1) {
        logisticMap<Nit>(N, c_r, c_x, item_ct1);
      });

  dpct::get_default_queue().memcpy(h_x, c_x, N * sizeof(float)).wait();

  std::ofstream outfile;
  outfile.open("L08.csv");
  outfile << "rVal, xVal \n";
  for (int i = 0; i < N; i++) {
    outfile << h_r[i] << ", " << h_x[i] << "\n";
  }
  return 0;
}
